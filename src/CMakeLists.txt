add_library(PlotTools SHARED)
target_sources(PlotTools
  PRIVATE
  Env.cxx
  Histo1D.cxx
  Histo2D.cxx
  HistoGaus.cxx
  HistoNoiseOccupancy.cxx
  HistoOccupancy.cxx
  HistoProjection.cxx
  HistoRms.cxx
  PlotStyle.cxx
  PlotTool.cxx
  Process.cxx
  ReadFile.cxx
  )
target_include_directories(PlotTools PUBLIC ${PROJECT_SOURCE_DIR}/include)
