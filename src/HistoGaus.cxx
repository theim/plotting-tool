#include "HistoGaus.h"

///////////////////////////////////////////////////////////////////////////
/// Class for RMS histogram
HistoGaus::HistoGaus() :
Histo1D()
{
#ifdef DEBUG
    std::cout << "HistoGaus::HistoGaus()" << std::endl;
#endif
};

HistoGaus::~HistoGaus() {
#ifdef DEBUG
    std::cout << "HistoGaus::~HistoGaus()" << std::endl;
#endif
};

void HistoGaus::fit(TH1* h) {
#ifdef DEBUG
    std::cout << "HistoGaus::fit(TH1)" << std::endl;
#endif
    this->fitHisto(h);
}

double HistoGaus::getMean() {
#ifdef DEBUG
    std::cout << "HistoGauss:getMean()" << std::endl;
#endif
    return m_mean;
}

double HistoGaus::getSigma() {
#ifdef DEBUG
    std::cout << "HistoGauss:getSigma()" << std::endl;
#endif
    return m_sigma;
}
void HistoGaus::setParameters(TH2* h) {
#ifdef DEBUG
    std::cout << "HistoGaus::setParameters(TH2)" << std::endl;
#endif
    m_histo_type = "Histo1d";

    m_axis.histo.x.nbins = 6;
    m_axis.histo.x.low   = 0;
    m_axis.histo.x.high  = 6;

    m_axis.histo.y.nbins = 1;
    m_axis.histo.y.low   = 0;
    m_axis.histo.y.high  = 1;

    m_axis.histo.x.title = "Deviation from the Mean [#sigma] ";
    m_axis.histo.y.title = "Number of Pixels";
    m_axis.histo.z.title = "";
}

void HistoGaus::fillHisto(json& j) {
#ifdef DEBUG
    std::cout << "HistoGaus::fillHisto(j)" << std::endl;
#endif
    m_mean = m_f->GetParameter(1);
    m_sigma = m_f->GetParameter(2);
    int nbinsx = m_axis.data.x.nbins;
    int nbinsy = m_axis.data.y.nbins;
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            double tmp = j["Data"][col][row];
            int bin_gaus = whichSigma(tmp, m_mean, m_sigma, 1, 6);
            //m_h->AddBinContent(bin_gaus);
            m_h->Fill(bin_gaus-1);
        }
    }
};
void HistoGaus::fillHisto(TH2* h) {
#ifdef DEBUG
    std::cout << "HistoGaus::fillHisto(TH2)" << std::endl;
#endif
    m_mean = m_f->GetParameter(1);
    m_sigma = m_f->GetParameter(2);
    int nbinsx = h->GetNbinsX();
    int nbinsy = h->GetNbinsY();
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            double tmp = h->GetBinContent(col+1, row+1);
            int bin_gaus = whichSigma(tmp, m_mean, m_sigma, 1, 6);
            //m_h->AddBinContent(bin_gaus);
            m_h->Fill(bin_gaus-1);
        }
    }
};

void HistoGaus::drawHisto() {
#ifdef DEBUG
    std::cout << "HistoGaus::drawHisto()" << std::endl;
#endif
    Histo1D::drawHisto();

    const char *LabelName[6] = {"1","2","3","4","5",">5"};
    for (int i=1; i<=6; i++) m_h->GetXaxis()->SetBinLabel(i, LabelName[i-1]);
    m_h->GetXaxis()->LabelsOption("h");
    gStyle->SetPaintTextFormat(".0f");
    m_h->SetMarkerSize(1.8);
    m_h->SetMarkerColor(1);
    m_h->Draw("TEXT0 SAME");
    m_h->GetYaxis()->SetRangeUser(0,((m_h->GetBinContent(m_h->GetMaximumBin()))*1.25));
    m_c->Update();
};

json HistoGaus::getResult() {
#ifdef DEBUG
    std::cout << "HistoGaus::getResult()" << std::endl;
#endif
    json result = Histo1D::getResult();
    result["tuned"] = m_h->GetBinContent(1)+m_h->GetBinContent(2)+m_h->GetBinContent(3);
    result["mean"] = m_mean;
    result["sigma"] = m_sigma;

    return result;
}
