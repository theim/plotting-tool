//Example for plot style

#ifndef  __PLOTSTYLE_H
#define __PLOTSTYLE_H

#include <iostream>

#include "TROOT.h"
#include <TStyle.h>
#include <TCanvas.h>
#include <TPaveStats.h>
#include <TH1.h>
#include <TF1.h>
#include <TH2.h>
#include <TMarker.h>
#include <THStack.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>

TStyle* PlotStyle();

void SetPlotStyle();

#endif // __PLOTSTYLE_H
