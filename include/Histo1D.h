#ifndef __HISTO1D_H
#define __HISTO1D_H

#include "PlotTool.h"

#include <json.hpp>
using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;

#include <vector>

class Histo1D : public PlotTool
{
public:
    /// Constructor
    Histo1D();

    /// Deconstructor
    ~Histo1D();

    virtual void setParameters(json& /*data*/);
    virtual json getResult();

    void setPercent(double per) { m_percent=per; };

    json setGaus(TF1* /*i_f*/);
    json setOcc(/*HistoOccupancy* */);
    json setNoi(/*HistoNoiseOccupancy* */);
    json setRms(double mean, double rms);
    json setZero();
protected:
    virtual void buildHisto();
    virtual void setCanvas();
    virtual void drawHisto();

    virtual void setParameters(TH1* /*h*/);
    virtual void setParameters(TH2* /*h*/);

    /// Fill histogram from data
    virtual void fillHisto(json& /*j*/);
    virtual void fillHisto(TH1* /*h*/);
    virtual void fillHisto(TH2* /*h*/);

    /// Gaussian fit histogram
    virtual void fitHisto(TH1* /*h*/);

    std::vector <float> thresholdPercent(double /*gaussMean*/);

    double m_percent;
};

bool sortbysec(const std::pair<double, float> &/*a*/, const std::pair<double, float> &/*b*/);

#endif //__HISTO1D_H
