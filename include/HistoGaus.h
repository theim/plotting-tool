#ifndef __HISTOGAUS_H
#define __HISTOGAUS_H

#include "Histo1D.h"

class HistoGaus : public Histo1D
{
public:
    HistoGaus();
    ~HistoGaus();

    void fit(TH1* /*h*/);
    double getMean();
    double getSigma();

    json getResult();
private:
    void setParameters(TH2* /*h*/);

    /// Fill histogram from data
    void fillHisto(json& /*j*/);
    void fillHisto(TH2* /*h*/);

    /// Draw histogram on the canvas
    void drawHisto();

    double m_mean;
    double m_sigma;
    //[[maybe_unused]] double m_chi;
};

#endif //__GAUS_H
