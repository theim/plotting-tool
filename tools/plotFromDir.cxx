#include <math.h> //for fabs()
#include <utility> //for pairs
#include <vector>
#include <algorithm> //for std::sort()
#include <iomanip>
#include <unistd.h>

#include <TStyle.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TH1.h>
#include <TF1.h>
#include <TH2.h>
#include <TPaveStats.h>
#include <TMarker.h>
#include <THStack.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>

#include "storage.hpp"
#include <json.hpp>
using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;

#include "PlotStyle.h"
#include "PlotTool.h"
#include "Histo1D.h"
#include "Histo2D.h"
#include "Process.h"

void printHelp();

///////////////////////////////////////////////////////////////////////////
// main function
int main(int argc, char *argv[]) {

    SetPlotStyle();

    /// variables
    std::string i_dir  = "";
    std::string o_dir    = "";
    std::string ext       = "png";
    std::string par_file   = "";
    bool doPrint = false;

    // get argument
    int c;
    while ((c = getopt(argc, argv, "HhPi:o:e:p:")) != -1) {
        switch (c) {
            case 'H':
                printHelp();
                return 0;
                break;
            case 'h':
                printHelp();
                return 0;
                break;
            case 'P':
                doPrint = true;
                break;
            case 'i':
                i_dir = std::string(optarg);
                break;
            case 'o':
                o_dir = std::string(optarg);
                break;
            case 'e':
                ext = std::string(optarg);
                break;
            case 'p':
                par_file = std::string(optarg);
                break;
            case '?':
                if(optopt=='o'||optopt=='e'||optopt=='p'){
                    std::cerr <<
                        "-> Option " <<
                        (char)optopt <<
                        " requires a parameter! (Proceeding with default)" <<
                    std::endl;
                }else if(optopt=='i'){
                    std::cerr <<
                        "-> Option " <<
                        (char)optopt <<
                        " requires a parameter! Aborting... " <<
                    std::endl;
                    return -1;
                } else {
                    std::cerr << "-> Unknown parameter: " << (char)optopt << std::endl;
                }
                break;
            default:
                std::cerr << "-> Error while parsing command line parameters!" << std::endl;
                return -1;
        }
    }

    /// Input directory
    if (i_dir=="") {
        std::cerr << "Error: No result directory given!" << std::endl;
        std::cerr << "       Please specify result directory path under -i option." << std::endl;
        return -1;
    }

    if (i_dir[i_dir.size()-1]=='/') i_dir = i_dir.substr(0, i_dir.size()-1);

    DIR *dp { nullptr };
    dp = opendir(i_dir.c_str());
    if ( !dp ) {
        std::cerr << "Error: Not found directory: " << i_dir << std::endl;
        return -1;
    }

    /// Output directory
    if (o_dir=="") {
        std::cout << "Warning: No output directory given. Proceeding with default: " << i_dir << std::endl;
        o_dir = i_dir;
    }

    if (o_dir[o_dir.size()-1]=='/') o_dir = o_dir.substr(0, o_dir.size()-1);

    std::string o_file = o_dir + "/rootfile.root";

    std::string cmd = "mkdir -p " + o_dir;
    dp = opendir(o_dir.c_str());
    if (system(cmd.c_str())!=0) {
        std::cerr << "Error: Problem in creating " << o_dir << std::endl;
        return -1;
    } else if (dp) {
        std::cout << "Warning: Already exist directory: " << o_dir << std::endl;
    }

    std::cout << std::endl;
    std::cout << "*********************************" << std::endl;
    std::cout << "Input directory: "  << i_dir << std::endl;
    std::cout << "Output directory: " << o_dir << std::endl;
    std::cout << "Output Root file: " << o_file << std::endl;
    if (doPrint) std::cout << "Extension: " << ext << std::endl;
    if (par_file!="") std::cout << "Parameter file: " << par_file << std::endl;
    std::cout << "*********************************" << std::endl;
    std::cout << std::endl;

    //////////////////
    // Check directory
    if (dirToRoot(i_dir, o_file)==-1) return -1;

    std::string i_file = o_file;
    analyzeRootFile(i_file, o_file, o_dir, doPrint, ext);

    std::cout << "Done." << std::endl;

    return 0;
}

void printHelp() {
    std::cout << "Help:" << std::endl;
    std::cout << " -h/-H     : Shows this." << std::endl;
    std::cout << " -P        : Set the print mode True." << std::endl;
    std::cout << " -i <dir>  : Result directory." << std::endl;
    std::cout << " -o <dir>  : Output directory. (Default. path/to/input/dir)" << std::endl;
    std::cout << " -e <ext>  : Extension. (Default. png)" << std::endl;
    std::cout << " -p <json> : Parameter config file." << std::endl;
}
