#include <math.h> //for fabs()
#include <utility> //for pairs
#include <vector>
#include <algorithm> //for std::sort()
#include <iomanip>
#include <unistd.h>

#include <TStyle.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TH1.h>
#include <TF1.h>
#include <TH2.h>
#include <TPaveStats.h>
#include <TMarker.h>
#include <THStack.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>

#include "storage.hpp"
#include <json.hpp>
using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;

#include "PlotStyle.h"
#include "PlotTool.h"
#include "Histo1D.h"
#include "Histo2D.h"
#include "Process.h"

#include "Env.h"

void printHelp();

///////////////////////////////////////////////////////////////////////////
// main function
int main(int argc, char *argv[]) {
    SetPlotStyle();

    std::string i_file = "";
    std::string o_dir = "";
    std::string ext = "png";
    std::string par_file = "";
    std::string chip_name = "";
    std::string data_name = "";
    std::size_t pos;

    // get argument
    int c;
    while ((c = getopt(argc, argv, "Hhi:o:e:p:k:c:d:")) != -1) {
        switch (c) {
            case 'H':
                printHelp();
                return 0;
                break;
            case 'h':
                printHelp();
                return 0;
                break;
            case 'i':
                i_file = std::string(optarg);
                break;
            case 'o':
                o_dir = std::string(optarg);
                break;
            case 'e':
                ext = std::string(optarg);
                break;
            case 'p':
                par_file = std::string(optarg);
                break;
            case 'c':
                chip_name = std::string(optarg);
                break;
            case 'd':
                data_name = std::string(optarg);
                break;
            case '?':
                if(optopt=='o'||optopt=='e'||optopt=='p'){
                    std::cerr <<
                        "-> Option " <<
                        (char)optopt <<
                        " requires a parameter! (Proceeding with default)" <<
                    std::endl;
                }else if(optopt=='i'){
                    std::cerr <<
                        "-> Option " <<
                        (char)optopt <<
                        " requires a parameter! Aborting... " <<
                    std::endl;
                    return -1;
                } else {
                    std::cerr << "-> Unknown parameter: " << (char)optopt << std::endl;
                }
                break;
            default:
                std::cerr << "-> Error while parsing command line parameters!" << std::endl;
                return -1;
        }
    }

    ///////////////////
    /// Input ROOT file
    if (i_file=="") {
        std::cerr << "Error: No ROOT file path given!" << std::endl;
        std::cerr << "       Please specify ROOT file path under -i option." << std::endl;
        return -1;
    }
    if (i_file.size()<6||i_file.substr(i_file.size()-5, i_file.size())!=".root") {
        std::cerr << "Error: Not ROOT file path: " << i_file << std::endl;
        std::cerr << "       Please specify ROOT file path under -i option." << std::endl;
        return -1;
    }

    ////////////////////
    /// Output directory
    if (o_dir=="") {
        pos = i_file.find_last_of('/');
        if (pos==std::string::npos) o_dir = ".";
        else o_dir = i_file.substr(0, pos);
        std::cout << "Warning: No output directory given. Proceeding with default: " << o_dir << std::endl;
    }

    std::string cmd = "mkdir -p " + o_dir;
    if (system(cmd.c_str())!=0) {
        std::cerr << "Error: Problem in creating " << o_dir << std::endl;
        return -1;
    }

    std::cout << std::endl;
    std::cout << "*********************************" << std::endl;
    std::cout << "Input ROOT file: "  << i_file << std::endl;
    std::cout << "Output directory: " << o_dir << std::endl;
    std::cout << "Extension: "        << ext << std::endl;
    if (par_file!="") std::cout << "Parameter file: "   << par_file << std::endl;
    std::cout << "*********************************" << std::endl;
    std::cout << std::endl;

    TFile* i_rootfile = new TFile(i_file.c_str(), "READ");
    Env* e;

    ///////////
    /// scanLog
    e = new Env("scanLog", (TEnv*)i_rootfile->Get("scanLog"));
    json scan_log = e->getJson();
    delete e;

    std::string chip_type = scan_log["chipType"];

    ///////////
    /// dataLog
    e = new Env("dataLog", (TEnv*)i_rootfile->Get("dataLog"));
    json data_log = e->getJson();
    delete e;

    /////////////
    /// Chip Name
    if (chip_name=="") {
        std::cout << "Chip List: " << std::endl;
        for (auto i : data_log["chips"]) {
            std::cout << "\t" << i["Name"].get<std::string>() << std::endl;
        }
        std::cout << "\nEnter the chip name whose data to plot. \n> ";
        std::cin >> chip_name;
    }
    int chip_num = 0;
    for (auto i : data_log["chips"]) {
        std::string name = i["Name"];
        if (chip_name!=name) {
            chip_num++;
            continue;
        } else {
            break;
        }
        std::cout << std::endl;
    }

    /////////////
    /// Data Name
    if (data_name=="") {
        json i = data_log["chips"][chip_num];
        std::cout << "Histogram List: " << std::endl;
        for (auto& k : i["plot"].items()) {
            std::cout << "\t" << k.key() << std::endl;
        }
        std::cout << "\nEnter the data name to plot. \n> ";
        std::cin >> data_name;
        std::cout << std::endl;
    }

    /////////
    /// Clone
    std::string key = chip_name + "_" + data_name;
    std::replace(key.begin(), key.end(), '-', '_');
    TH1* h = (TH1*)i_rootfile->Get(key.c_str());
    std::string data_type = data_log["chips"][chip_num]["plot"][data_name];

    std::cout << "Cloning " << key << " ( " << data_type << " ) from " << i_file << std::endl;

    std::unique_ptr<PlotTool> pH;
    if      (data_type=="Histo1d") pH.reset( new Histo1D );
    else if (data_type=="Histo2d") pH.reset( new Histo2D );
    else {
        std::cerr << "Something wrong" << std::endl;
        return 1;
    }
    pH->setChip(chip_type, chip_name);
    if      (data_type=="Histo1d") pH->setData((TH1*)h, data_name);
    else if (data_type=="Histo2d") pH->setData((TH2*)h, data_name);
    delete h;

    /////////////////
    /// Set parameter
    if (par_file!="") {
        std::cout << "Setting parameters from " << par_file << std::endl;
        json p_j = fileToJson(par_file, "");
        pH->setParameters(p_j);
    }

    ///////////////////
    /// Build histogram //TODO It takes long time.
    std::cout << "Creating " << o_dir << "/" << key << "." << ext << std::endl;
    pH->build();

    //////////////////
    /// parameter file
    json par_json = pH->getParameters();
    std::string title = o_dir + "/" + chip_name + "_" + data_name + ".json";
    std::ofstream tmpfile(title.c_str());
    tmpfile << std::setw(4) << par_json;
    tmpfile.close();

    //////////
    /// Output
    pH->print(o_dir, ext);
    pH.reset();
    std::cout << "Done." << std::endl;
    i_rootfile->Close();

    return 0;
}

void printHelp() {
    std::cout << "Help:" << std::endl;
    std::cout << " -h/-H     : Shows this." << std::endl;
    std::cout << " -i <file> : Input ROOT file." << std::endl;
    std::cout << " -o <dir>  : Output directory. (Default. path/to/input/dir)" << std::endl;
    std::cout << " -e <ext>  : Extension. (Default. png)" << std::endl;
    std::cout << " -p <json> : Parameter config file." << std::endl;
    std::cout << " -c <chip> : Chip name listed in the ROOT file." << std::endl;
    std::cout << " -d <data> : Data name listed in the ROOT file." << std::endl;
}
